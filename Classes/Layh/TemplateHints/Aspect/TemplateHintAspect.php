<?php
namespace Layh\TemplateHints\Aspect;

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Core\Bootstrap;
use TYPO3\Flow\Http\Request;

/**
 * TemplateHintAspect
 *
 * @Flow\Aspect
 */
class TemplateHintAspect {

	/**
	 * @var \TYPO3\Flow\Core\Bootstrap
	 * @Flow\Inject
	 */
	protected $bootstrap;

	/**
	 * @var \TYPO3\Flow\Mvc\Routing\Router
	 * @Flow\Inject
	 */
	protected $router;

	/**
	 * Aspect that is wrapped around the renderer to get the template hints
	 *
	 * @Flow\Around("method(TYPO3\Fluid\ViewHelpers\RenderViewHelper->render())")
	 *
	 * @param \TYPO3\Flow\Aop\JoinPointInterface $joinPoint
	 * @return string $content
	 */
	public function showTemplateHintsRender(\TYPO3\Flow\Aop\JoinPointInterface $joinPoint) {

		$content = $joinPoint->getAdviceChain()->proceed($joinPoint);

		if (!isset($_GET['TH'])) {
			return $content;
		}

		$phpStormLink = $this->buildLink($joinPoint->getMethodArguments());

		if (!empty($content)) {
			$result = '<div onmouseout="hideHoverElement(this)" onmouseover="showHoverElement(this)" class="templateHintsBox" style="border:1px dashed red" title="' . $phpStormLink . '">';
			$result .= '<div class="templateHintsWindow"><a onclick="getHttpRequest(\'' . $phpStormLink . '\'); return false;" href="#">Open File</a></div>';
			$result .= $content;
			$result .= '</div>';
		}

		return $result;
	}

	/**
	 * @param $arguments
	 * @return string
	 */
	protected function buildLink($arguments) {

		/** @var \TYPO3\Flow\Http\RequestHandler $request */
		$request = $this->bootstrap->getActiveRequestHandler();
		$actionRequest = $this->router->route($request->getHttpRequest());

		$activePackage = $actionRequest->getControllerPackageKey();
		$activeController = $actionRequest->getControllerName();
		$activeAction = $actionRequest->getControllerActionName();

		if ($this->isPartial($arguments)) {
			$endPartOfPath = 'Partials/' . $arguments['partial'] . '.html';
		} else {
			$endPartOfPath = 'Templates/' . $activeController . '/' . ucfirst($activeAction) . '.html';
		}

		$phpStormLink = 'http://localhost:8091/?message=Packages/Application/' . $activePackage . '/Resources/Private/' . $endPartOfPath;
		return $phpStormLink;
	}

	/**
	 * @param $arguments
	 * @return bool
	 */
	protected function isPartial($arguments) {
		if ($arguments['partial'] != NULL) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

}
?>