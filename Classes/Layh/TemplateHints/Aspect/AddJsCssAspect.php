<?php
namespace Layh\TemplateHints\Aspect;

use TYPO3\Flow\Annotations as Flow;
/**
 * AddJsCssAspect
 *
 * @Flow\Aspect
 */
class AddJsCssAspect {

	/**
	 * Aspect that is wrapped around the renderer to get the template hints
	 *
	 * @Flow\Around("method(TYPO3\Fluid\View\AbstractTemplateView->render())")
	 *
	 * @param \TYPO3\Flow\Aop\JoinPointInterface $joinPoint
	 * @return string $content
	 */
	public function addJsCssInAbstractView(\TYPO3\Flow\Aop\JoinPointInterface $joinPoint) {

		$content = $joinPoint->getAdviceChain()->proceed($joinPoint);

		if (!isset($_GET['TH'])) {
			return $content;
		}

		$content = str_replace('</body>', '', $content);
		$content = str_replace('</html>', '', $content);

		if (!empty($content)) {
			$content .= $this->getCss();
			$content .= $this->getJs();
		}

		$content = $content . '</body></html>';

		return $content;
	}

	/**
	 * @return string
	 */
	protected function getJs() {
		$js = "
			<script type='text/javascript'>

			function showHoverElement(elem) {
				var innerElement;
				innerElement = elem.getElementsByClassName('templateHintsWindow');
				innerElement[0].style.display = 'block';
			}

			function hideHoverElement(elem) {
				var innerElement;
				innerElement = elem.getElementsByClassName('templateHintsWindow');
				innerElement[0].style.display = 'none';
			}

			function getHttpRequest(link) {
				var xmlHttp = null;
				// Mozilla
				if (window.XMLHttpRequest) {
					xmlHttp = new XMLHttpRequest();
				}
				// IE
				else if (window.ActiveXObject) {
					xmlHttp = new ActiveXObject('Microsoft . XMLHTTP');
				}

				xmlHttp.open('GET', link, true);
				xmlHttp.onreadystatechange = function() {
					// nothing to do here
				};
				xmlHttp.send(null);
			}

			</script>
		";
		return $js;
	}


	/**
	 * @return string
	 */
	protected function getCss() {
		$css = '
			<style type="text/css">
				.templateHintsWindow {
					display:none;
					position: relative;
					top: 0px;
					left: 0px;
					z-index: 1000;
					width: 80px;
					text-align:center;
					background-color: lightblue;
				}
			</style>
		';
		return $css;
	}

}
?>