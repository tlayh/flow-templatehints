TYPO3.Flow Package TemplateHints
-----------------------------------------

This package was inspired by the idea of Fabrizio's Package Aoe_TemplateHints[1] for Magento.

To use it, just clone the repository to Packages/Applications and activate the package.

To display the template hints, add the parameter TH=1 to the end of your URL.



[1] https://github.com/fbrnc/Aoe_TemplateHints